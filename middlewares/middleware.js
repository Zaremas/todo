const jwt = require('jsonwebtoken');

const config = require('../config')


async function verifyToken(req, res, next) {
  const token = req.headers['authorization'];
  if (!token) {
    return res.status(401).json({ error: 'Unauthorized' });
  }
  
  jwt.verify(token, config.jwtkey, (err, decoded) => {
    if (err) {
      res.status(401).json({ error: 'Unauthorized' });
      return next(err)
    }
    req.user = decoded;
    return next();
  });
}

const fs = require('fs');
const path = require('path');

async function errorLogger(err, req, res, next) {
  const errorLogPath = path.join(__dirname, '../logs/error.log');
  const errorMessage = `${new Date().toISOString()}: ${err.stack}\n`;
  
  fs.appendFile(errorLogPath, errorMessage, (error) => {
    if (error) {
      console.error('Error writing to error log:', error);
    }
  });

  return next(err);
}

module.exports = { verifyToken , errorLogger};