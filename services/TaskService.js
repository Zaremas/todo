
const UserModel = require("../models/User");

class TaskService {
  static async getTasks(email, areDone) {
    const user = await this.getUser(email);

    const tasks = user.tasks;

    const filtered = tasks.filter((task) => {
      return task.done === areDone;
    });
    return filtered;
  }
  static async getUser(email) {
    const [user] = await UserModel.find({ email: email }).exec();
    return user;
  }

  static async setDone(id, email) {
    const user = await this.getUser(email)
    await UserModel.findOneAndUpdate(
        { _id: user._id, 'tasks._id': id }, 
        { $set: {'tasks.$.done': true}}
    )
    .catch(()=> {
        return Promise.reject(
        new Error("Task or user not found when trying to change data")
      );
    });
  }

  static async saveData(title, desc, email) {
    const taskObj = {
      title: title,
      description: desc,
      done: false,
    };

    const user = await this.getUser(email);
    await user.tasks.push(taskObj);
    await user.save();
  }

  static async changeData(id, title, desc, email) {
    const user = await this.getUser(email)
    await UserModel.findOneAndUpdate(
        { _id: user._id, 'tasks._id': id }, 
        { $set: {'tasks.$.title': title,'tasks.$.description': desc}}

    )
    .catch(()=> {
        return Promise.reject(
        new Error("Task or user not found when trying to change data")
      );
    });
  }

  static async deleteTask(id, email) {
    const user = await this.getUser(email)
    await UserModel.findOneAndUpdate(
        { _id: user._id}, 
        { $pull: { tasks: { _id: id } }}
    ).catch(()=> {
        return Promise.reject(
        new Error("Task or user not found when trying to change data")
      );
    });
  }
}

module.exports = TaskService;
