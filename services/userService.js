const UserModel = require('../models/User');
class UserService {
  static async getAll() {
    return UserModel.find({}).sort({ createdAt: -1 });
  }

  static async getOne(email) {
    return UserModel.find({email:email}).exec();
  }

  static async create(data) {
    const user = new UserModel(data);
    return user.save();
  }

  static async update(email, data) {
    // Fetch the user first
    const user = await UserModel.find({email:email});
    
    user.password = data.password;

    return user.save();
  }

  static async remove(userId) {
    return UserModel.deleteOne({ _id: userId }).exec();
  }
  static async comparePassword(password){
    return UserModel.comparePassword(password)
  }
  static async validateCredentials(email, password){
    const [user] = await this.getOne(email);
        if (!user) {
            return false;
        }
        const isValidPassword = await user.comparePassword(password);
        if (!isValidPassword) {
            return false;
        }
        return true
  }
}

module.exports = UserService;