const express = require("express");
const mongoose = require("mongoose");
const makeStoppable = require("stoppable")
const http = require("http");
const path = require('path');
const bs = require("body-parser");

const routes = require('../routes');
const authRouter = require('../routes/authRoute');
const {errorLogger} = require('../middlewares/middleware'); 
const config = require('../config')

const app = express();


async function connectToMongoose() {
  return mongoose.connect(config.mongodb.url);
}

connectToMongoose()
  .then(() => {
    console.info("connected to mongodb");
  })
  .catch((err) => {
    console.error(err);
  });

app.set('trust proxy', 1);


const server = makeStoppable(http.createServer(app));
app.set('view engine', 'ejs');
app.set('views',  path.join(__dirname, '../views'))
app.use(express.static(path.join(__dirname, '../assets')));
app.use(bs.json());
app.use(
  '/api/tasks',routes()
)
app.use(
  '/api/auth/', authRouter()
)

app.use(errorLogger);

module.exports = () => {

  

  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    })
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
}
