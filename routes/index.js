const express = require("express");
const { verifyToken } = require('../middlewares/middleware');

const { check, validationResult } = require("express-validator");
const TaskService = require("../services/TaskService")
const jwt = require('jsonwebtoken');
const router = express.Router();

const validation = [check("title").trim().isLength({ min: 3 }).escape()];

module.exports = () => {

  router.get("/", verifyToken, async (request, response, next) => {
    try {
      const tasks = await TaskService.getTasks(request.user.email,false);
      console.log("from index",tasks);
      response.status(200)
      return response.render("layout", {
        pageTitle: "Tasks",
        template: "index",
        tasks,
      });
    } catch (err) {
      return next(err);
    }
  });

  router.post("/", validation, verifyToken, async (request, response, next) => {
    try {
      const errors = validationResult(request);

      if (!errors.isEmpty()) {
        return response.redirect("/");
      }
      let { title, description } = request.body;

      await TaskService.saveData(title, description, request.user.email);
      return response.status(201).json({'message':"created task"})
    } catch (err) {
      return next(err);
    }
  });

  router.put('/:taskid',validation, verifyToken, async (request, response, next) => {
    try {
      const errors = validationResult(request);
      if (!errors.isEmpty()) {
        return response.status(400)
      }
      await TaskService.changeData(request.params.taskid, request.body.title, request.body.description,request.user.email);
      return response.status(200).json({"message":"Task changed"});
    } catch (err) {
      return next(err);
    }
  });

  router.post("/done", verifyToken,async (request, response, next) => {
      try {
        await TaskService.setDone(request.body.taskid, request.user.email);

        return response.status(200).json({"message":"Task marked as done"})
      } catch (err) {
        return next(err); 
      }
    }
  );

  router.get('/done',verifyToken, async (request, response, next) => {
    try {
      const tasks = await TaskService.getTasks(request.user.email,true);
      return response.render('layout', {
        pageTitle: 'Done',
        template: 'done',
        tasks,
      });
    } catch (err) {
      return next(err);
    }
  });

  router.delete("/", verifyToken, async(request, response, next) => {
    try {
      await TaskService.deleteTask(request.body.taskid,request.user.email);
      return response.status(200).json({"message":"Task deleted"})
    } catch (err) {
      return next(err);
    }
  } )

  return router;
};
