const express = require('express');
const jwt = require('jsonwebtoken');

const UserService = require("../services/userService");
const config = require('../config')


const router = express.Router();

module.exports = () => { 

    router.post("/register", async (req, res, next) => {
      try{
        const { email, password, firstName, lastName } = req.body;
        
        [foundUser] = await UserService.getOne(email)
        if (foundUser) {
          return res.status(400).json({ error: 'Email already exists' });
        }

        await UserService.create({ email, password, firstName, lastName })
        
        res.status(201).json({ message: 'User registered successfully' });
      }
      catch(err){
        return next(err);
      }
    
    });

    router.post("/login",async (req, res, next) => {
      try{
        const { email, password } = req.body;
    

        if (!await UserService.validateCredentials(email, password)){
          return res.status(401).json({ error: 'Invalid email or password' });
        }
    
        jwt.sign({email}, config.jwtkey, { expiresIn: '10h' },(err, token) => {
            if(err) { next(err) };    
            res.send(token);
        })}
        catch(err){
          return next(err);
        }
    });
  

  return router;
};
